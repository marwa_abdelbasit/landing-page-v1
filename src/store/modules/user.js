import axios from "axios";

const state = {
    user: localStorage.getItem('user_data') || '',
    token: localStorage.getItem('access_token') || '',
    isLogged: false 
}

const getters = {
    getUser: state => JSON.parse(state.user),
    isLogged: state => state.isLogged,
    isAuthenticated: state => state.token
}

const actions = {
    loginAction(context, user) {
        axios.post("https://skepadmin.com/api/Auth/Login", {
            email: user.email,
            password: user.password
        })
        .then(res => {
            if(res.data.access_token) {
                localStorage.setItem('access_token', res.data.access_token)
                localStorage.setItem('user_data', JSON.stringify(res.data.UserData))
                context.commit('logUser')
                window.location.replace('/dashboard')
            }
        })
        .catch(err => {
            console.log(err.message);
        })
    }
}


const mutations = {
    logUser: state => {
        state.isLogged = true
    }
}

export default {
    namespace: true,
    state,
    getters,
    mutations,
    actions
}