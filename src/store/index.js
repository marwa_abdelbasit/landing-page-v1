import Vue from 'vue'
import Vuex from 'vuex'
import user from './modules/user'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    spinner: false,
    isToggled: false,
  },
  mutations: {
    setSpinner: (state, spinner) => {
      state.spinner = spinner
    },
    setisToggled: (state, isToggled) => {
      state.isToggled = isToggled
    }
  },
  getters: {
    getSpinner: (state) => {
      return state.spinner
    },
    getisToggled: state => {
      return state.isToggled
    }
  },
  actions: {
    loading: ({commit}) => {
      commit("setSpinner", true)
      setTimeout(() => {
        commit("setSpinner", false)
      }, 1000)
    },
    openSideAction: ({commit}, isToggled) => {
      commit("setisToggled", isToggled)
    }
  },
  modules: {
    user,
  }
})
